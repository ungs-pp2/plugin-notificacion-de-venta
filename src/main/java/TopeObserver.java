import com.grupo6.business.Tope;
import com.grupo6.business.TotalDelDiaBusiness;

import javax.swing.*;
import java.util.Observable;

public class TopeObserver implements Tope {

    private double topeDelDia;

    private boolean isNotificado;

    public TopeObserver() {
        isNotificado = false;
    }

    @Override
    public Tope topeDelDia(double topeDelDia) {
        this.topeDelDia = topeDelDia;
        return this;
    }

    @Override
    public void update(Observable observable, Object arg) {
        TotalDelDiaBusiness totalDelDiaBusiness = (TotalDelDiaBusiness) observable;
        double total = ((TotalDelDiaBusiness) observable).getTotalDelDia();
        if(total >= topeDelDia && isNotificado == false){
            JOptionPane.showMessageDialog(null, "Barbaro alcanzaste el objetivo: "
                    + totalDelDiaBusiness.getTotalDelDia());
            isNotificado = true;
        }
    }
}
